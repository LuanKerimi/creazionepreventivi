<x-layout>
    <x-navbar />

    {{-- TASK
        Configurazione tabella prodotti popolata con i dati presenti nel file excel
        Configurazione tabella coefficienti popolata con i dati presenti nel file excel
        Configurazione tabella in cui vengano salvati i preventivi dove dovranno essere presenti il
        riferimento univoco del prodotto selezionato e del coefficiente selezionato

        -Tutti i contratti hanno un deposito cauzionale pari al 10% del valore del contratto di noleggio (un
        deposito infruttifero posto a garanzia del pagamento dei canoni restituito a fine contratto)
        - Il Valore del contratto di noleggio è dato dal prodotto del canone calcolato nel preventivo per la
        durata scelta nel preventivo
        - I canoni che vanno dai 12 ai 60 mesi sono calcolati in questo modo: Valore del
        bene*1,3*coefficiente/100 --}}



    @if (session('message'))
        <div class="alert  alert-danger text-center">
            {{ session('message') }}
        </div>
    @endif

    <div class="container">
        <h1 class="text-center my-5">Crea preventivi</h1>
        <div class="row ">
            <form method="POST" action="{{ route('storeQuotation') }}" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-3">
                        <h5 class="ms-3">Seleziona i mesi</h5>
                        <select class="form-select form-select-lg mb-3" aria-label=".form-select-lg example"
                            name="coefficient">
                            <option selected>Mesi</option>
                            @foreach ($coefficientList as $coefficient)
                                <option value="{{ $coefficient->id }}">{{ $coefficient->months }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-9">
                        <h5 class="ms-3">Seleziona il prodotto</h5>
                        <select class="form-select form-select-lg mb-3" aria-label=".form-select-lg example"
                            name="product">
                            <option selected>Prodotto</option>
                            @foreach ($productsList as $product)
                                <option value="{{ $product->id }}">{{ $product->model }} | {{ $product->price }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                {{-- <input type="text" name="message"> --}}
                <button type="submit" class="btn btn-success mt-2">Crea Preventivo</button>
            </form>
            <div class="col-4">
                <a href="{{ route('quotationIndex') }}"><button class="btn btn-primary my-4">Lista Preventivi
                    </button></a>
            </div>


        </div>
    </div>



    <div class="container">
        <button class="btn btn-primary" type="button" data-bs-toggle="collapse" data-bs-target="#collapseExample"
            aria-expanded="false" aria-controls="collapseExample">
            Ultimi Preventivi
        </button>
        <div class="collapse" id="collapseExample">
            <div class="row">
                <div class="col-12">
                    <div>
                        <table class="table table-bordered table-hover table-sm">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Categoria</th>
                                    <th scope="col">Modello</th>
                                    <th scope="col">Prezzo</th>
                                    <th scope="col">Valore Contratto</th>
                                    <th scope="col">Rata</th>
                                    <th scope="col">Deposito</th>
                                    <th scope="col">Mesi/Coeff</th>
                                    <th scope="col">Creato</th>
                                    <th scope="col">INFO</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($quotationsList as $quotation)

                                    <th scope="row">{{ $quotation->id }}</th>
                                    <td>{{ $quotation->product->category }}</td>
                                    <td>{{ $quotation->product->model }}</td>
                                    <td>{{ $quotation->product->price }}</td>
                                    <td>{{ $quotation->contract_value }}</td>
                                    <td>{{ $quotation->price_month }}</td>
                                    <td>{{ $quotation->security_deposit }}</td>
                                    <td>{{ $quotation->months }}/{{ $quotation->coefficient }}</td>
                                    <td>{{ $quotation->created_at->format('d/m/Y') }}</td>
                                    <td class="col-2">
                                        <div class="collapse multi-collapse" id="atQuot{{ $quotation->id }}">

                                            <p>{{ $quotation->product->features }}</p>
                                            <h6>PN</h6>
                                            <p>{{ $quotation->product->PN }}</p>
                                        </div>
                                        <div>
                                            <a class="btn btn-primary m-2" data-bs-toggle="collapse"
                                                href="#atQuot{{ $quotation->id }}" role="button"
                                                aria-expanded="false"
                                                aria-controls="multiCollapseExample1">Mostra/nascondi</a>
                                        </div>
                                    </td>
                                    <div class="collapse multi-collapse" id="quotation{{ $quotation->id }}">
                                    </div>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">

        <div class="row">
            <div class="col-12 col-md-7 ">

                <div class="panel">
                    {{-- <div class="panel-heading">Pie Chart Integration in Laravel 8</div> --}}
                    <div class="panel-body">
                        <div id="kpi-product"></div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-5 ">

                <div class="panel">
                    {{-- <div class="panel-heading">Pie Chart Integration in Laravel 8</div> --}}
                    <div class="panel-body">
                        <div id="kpi-month"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>
    <script>
        $(function() {
            Highcharts.chart('kpi-product', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'KPI - Prodotti'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                accessibility: {
                    point: {
                        valueSuffix: '%'
                    }
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: <br> unità : {point.count} </br>  {point.percentage:.1f} %'
                        }
                    }
                },
                series: [{
                    name: 'Product',
                    colorByPoint: true,
                    data: <?= $data ?>
                }]
            });
        });

        $(function() {
            Highcharts.chart('kpi-month', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'KPI - Mesi'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                accessibility: {
                    point: {
                        valueSuffix: '%'
                    }
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b> Mesi:  {point.name}</b>: <br> Contratti : {point.count} </br>  {point.percentage:.1f} %'
                        }
                    }
                },
                series: [{
                    name: 'Month',
                    colorByPoint: true,
                    data: <?= $dataM ?>
                }]
            });
        });
        
    </script>

</x-layout>
