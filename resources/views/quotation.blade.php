
<x-layout>
    <x-navbar/>

    <div class="row mb-4 mt-3 align-items-center">
        <div class="col-12 col-md-8 offset-md-2 p-4 ">
          @if (session('message')) 
          <div class="alert  alert-danger text-center"> 
            {{ session('message') }}
          </div>       
          @endif
        </div>
      </div>

<div class="container">
    <div class="row">
        <div><h2 class="text-center my-4">Lista Preventivi</h2></div>
        <div class="col-12">
            <div>
                <table class="table table-bordered table-hover table-sm">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Categoria</th>
                            <th scope="col">Modello</th>
                            <th scope="col">Prezzo</th>
                            <th scope="col">Valore Contratto</th>
                            <th scope="col">Rata</th>
                            <th scope="col">Deposito</th>
                            <th scope="col">Mesi/Coeff</th>
                            <th scope="col">Creato</th>
                            <th scope="col">INFO</th>
                        
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($quotationsList as $quotation)
                            <th scope="row">{{ $quotation->id }}</th>
                            <td>{{ $quotation->product->category }}</td>
                            <td>{{ $quotation->product->model }}</td>
                            <td>{{ $quotation->product->price }}</td>
                            <td>{{ $quotation->contract_value }}</td>
                            <td>{{ $quotation->price_month }}</td>
                            <td>{{ $quotation->security_deposit }}</td>
                            <td>{{ $quotation->months }}/{{$quotation->coefficient}}</td>
                            <td>{{ $quotation->created_at->format('d/m/Y') }}</td>
                            <td class="col-2">
                                <div class="collapse multi-collapse" id="atQuot{{ $quotation->id }}">
                                    <p>{{ $quotation->product->features }}</p>
                                    <h6>PN</h6>
                                    <p>{{ $quotation->product->PN }}</p>
                                    <form method="POST" action="{{route('deleteQuotation', compact('quotation'))}}">
                                        @csrf
                                        @method('delete')
                                        <button type="submit" class="btn btn-danger m-1">Elimina Preventivo</button>
                                    </form>
                                </div>
                                <div>
                                    <a class="btn btn-primary m-2" data-bs-toggle="collapse"
                                        href="#atQuot{{ $quotation->id }}" role="button" aria-expanded="false"
                                        aria-controls="multiCollapseExample1">Mostra/nascondi</a>
                                </div>
                            </td>
                            <div class="collapse multi-collapse" id="quotation{{ $quotation->id }}">
                            </div>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</x-layout>