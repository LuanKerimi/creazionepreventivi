<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\QuotationController;
use App\Models\Quotation;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/', [ProductController::class, 'index'])->name('product');
Route::post('/quotation', [QuotationController::class, 'store'])->name('storeQuotation');
Route::get('/quotation/list', [QuotationController::class, 'index'])->name('quotationIndex');
Route::delete('/delete/{quotation}', [QuotationController::class, 'destroy'])->name('deleteQuotation');
Route::get('welcome', [ProductController::class, 'quotationIndex']);