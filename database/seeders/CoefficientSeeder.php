<?php

namespace Database\Seeders;

use App\Models\Product;
use App\Models\Coefficient;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CoefficientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      
        $csv = array();
        if (($handle = fopen("./resources/coefficienti.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $csv[] = $data;
            }
        }

        $month = $csv[2];
        $coefficiente = $csv[3];

        for ($i = 0; $i < sizeof($month); $i++) {
            if ($i != 0) {
                DB::table('coefficients')->insert([
                    "months" => $month[$i],
                    "coefficiente" => $coefficiente[$i],

                ]);
            }
        }

        fclose($handle);
    }
}
