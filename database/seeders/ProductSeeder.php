<?php

namespace Database\Seeders;



use App\Models\Product;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    
    public function run()
    {

        $csvFile = fopen(("resources/listino.csv"), "r");
        $firstline = true;
                       
        while (($data = fgetcsv($csvFile, 2000, ",")) !== FALSE) {
            if(!$firstline){   
                              
                DB::table('products')->insert([
                     "category" => $data['0'],
                     "model" => $data['1'],
                     "PN" => $data['2'],
                     "features" => $data['3'],
                     "price" => $data['4'],
                ]);
            }

            $firstline = false;
            
        }

        fclose($csvFile);
    }
}


    
    

