<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Quotation extends Model
{
    use HasFactory;

    protected $fillable = [
        'product_id','contract_value','price_month', 'security_deposit', 'coefficient_id', 'months', 'coefficient'
        
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
