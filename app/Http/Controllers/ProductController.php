<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Quotation;
use App\Models\Coefficient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coefficientList = Coefficient::all();
        $productsList  = Product::all();
        $quotationsList = Quotation::all()
            ->sortDesc()
            ->take(5);

        $quotationAll = Quotation::all();

        $kpiMonth = DB::table('quotations')
            ->select('months', DB::raw('count(*) as total'))
            ->groupBy('months')
            ->get();

        $kpiProduct = Product::withCount('quotations')
            ->orderBy('quotations_count')
            ->get()
            ->sortByDesc('quotations_count');

        $dataProduct = [];

        foreach ($kpiProduct as $quotation) {
            $dataProduct[] = [
                "name" => $quotation['model'],
                "y" => $quotation['quotations_count'],
                "count" => $quotation['quotations_count']
            ];
        }

        $dataMonth = [];

        foreach ($kpiMonth as $month) {
            $dataMonth[] = [
                "name" => $month->months,
                "y" =>  $month->total,
                "count" => $month->total,
            ];
        }



        return view(
            'welcome',
            [
                "data"  => json_encode($dataProduct),
                "dataM" => json_encode($dataMonth)
            ],

            compact('productsList', 'coefficientList', 'quotationsList',)
        );
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }
}
