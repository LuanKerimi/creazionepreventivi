<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Quotation;
use App\Models\Coefficient;
use Illuminate\Http\Request;

class QuotationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $quotationsList= Quotation::all();

        return view('quotation', compact( 'quotationsList', ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product = Product::find($request->product);
        $coefficient = Coefficient::find($request->coefficient);

        $coeff = str_replace(",", ".", $coefficient->coefficiente);
        $num = str_replace(".", "", $product->price);
        $num = str_replace(",", ".", $num);
        $num = (double) $num;

        $canon = number_format((double)($num * 1.3 * $coeff / 100), 2, '.', '');
        $contractValue = $canon * $coefficient->months;
        $securityDeposit = number_format((double)($contractValue * 0.10), '2', '.', '');

        $canon = str_replace(".", ",", $canon);
        $contractValue = str_replace(".", ",", $contractValue);
        $securityDeposit = str_replace(".", ",", $securityDeposit);

        $product->quotations()->create([
            'coefficient_id' => $coefficient->id,
            'months' => $coefficient->months,
            'coefficient' => $coefficient->coefficiente,
            'price_month' => $canon . " €",
            'contract_value' => $contractValue . " €",
            'security_deposit' => $securityDeposit . " €"

        ]);


        return redirect()->back()->with('message', "Il tuo preventivo è stato creato");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Quotation  $quotation
     * @return \Illuminate\Http\Response
     */
    public function show(Quotation $quotation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Quotation  $quotation
     * @return \Illuminate\Http\Response
     */
    public function edit(Quotation $quotation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Quotation  $quotation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Quotation $quotation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Quotation  $quotation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Quotation $quotation)
    {
        $quotation->delete();
        return redirect()->back()->with('message', "Il tuo preventivo è stato eliminato");

    }
}
