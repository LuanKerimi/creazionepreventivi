

    

-Ho preso le tabelle .csv e le ho inserite in resources\coefficienti.csv
-La logica del riempimento del DB la potete trovare in  database\seeders\CoefficientSeeder.php
                                                        database\seeders\ProductSeeder.php

- in app\Http\Controllers\ProductController.php ho inserito la logica per l'index dei prodotti, che vengono mostrati nella home
  così da poter essere utilizzati per la creazione dei preventivi
- in app\Http\Controllers\QuotationController.php troverete la logica per la creazione/eliminazione dei preventivi e l'index
  ho deciso di arrotondare a soli due cifre dopo la virgola e ripristinare le virgole nel DB, in modo da mantenere il modello
- nella home page ho deciso di inserire anche gli ultimi 5 preventivi creati, ordinati dal più recente, per riempirla un po
- ho aggiunto una navbar per muoversi piu velocemente da una pagina all'altra
- nella pagina resources\views\quotation.blade.php c'è la lista di tutti i preventivi, in ordine di creazione (primo/ultimo)

-considerazioni personali: il progetto è stato interessante e divertente, non avevo mai lavorato con un DB esterno ne riordinato i dati, 
 inizialmente avevo pensato di creare una tabella pivot con una relazione Many to Many, ero riuscito a creare le relazioni tra
 i modelli Coefficient e Product, andando a creare e riempire le tabelle nel pivot (attach with pivot ecc), ma ho avuto dei problemi nel richiamare la relazione del coefficiente nelle pagine, alla fine ho deciso di creare una one to many e inserire a mano i dati del coefficiente,considerando anche che erano solo due campi,



